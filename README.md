# Oersi-Admin

##### Spring Boot Admin is a web application, used for managing and monitoring Spring Boot applications. Each application is considered as a client and registers to the admin server. Behind the scenes, the magic is given by the Spring Boot Actuator endpoints.

### This is a demo for Oersi-s Applications
##-----------------------------------------------------
## Configuration  

##### * Oersi-Admin have default :
 *     port:8090 

##### * In properties need to make some configuration :

*  __email configuration__

>              mail.default-encoding=UTF-8
>              mail.host=smtp.gmail.com
>              mail.port=587
>              mail.username=<Username>
>              mail.password=<Password>
>              mail.smtp.auth=true
>              mail.send.from=<Email-From>
>              mail.send.to=<Email-To>
>              mail.starttls.enable=true
>              mail.starttls.required=true
>              mail.template=true
>              mail.template.path=templates/
>              mail.template.name=status-change
>              mail.default.language=de
>              mail.default.link=localhost:8081
>              mail.send.subject.Default=Server Status 

*  __Spring Boot admin Configuration__
>              spring.application.name=spring-boot-admin-server
*  __Username Password For Login__
>              spring.security.user.name=admin
>              spring.security.user.password=admin
>
* __If for client you want to add security you need to add here and in Client project__
>              
>             spring.boot.admin.client.instance.metadata.user.name=client
>             spring.boot.admin.client.instance.metadata.user.password=client
>
##--------------------------------------------------------

#### Client Configuration

>            spring.application.name=oersi-backend
>             
>            spring.boot.admin.client.url=http://localhost:8090
>            management.endpoints.web.exposure.include=*
>            management.endpoint.health.show-details=always
>            spring.boot.admin.client.username=client
>            spring.boot.admin.client.password=client
>
* __ if you want to expose the log file to Admin __
>            management.endpoint.logfile.external-file=./logs/oersi.log
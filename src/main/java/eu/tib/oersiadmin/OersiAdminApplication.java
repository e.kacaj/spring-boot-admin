package eu.tib.oersiadmin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAdminServer
@SpringBootApplication
public class OersiAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(OersiAdminApplication.class, args);
    }

}

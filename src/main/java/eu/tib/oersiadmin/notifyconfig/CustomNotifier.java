package eu.tib.oersiadmin.notifyconfig;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.events.InstanceEvent;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.notify.AbstractEventNotifier;
import eu.tib.oersiadmin.emailservice.iemail.EmailSender;
import eu.tib.oersiadmin.emailservice.iemail.MessageService;
import eu.tib.oersiadmin.emailservice.iemail.Properties;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;

@Component
@Slf4j
public class CustomNotifier extends AbstractEventNotifier {

    private static final Logger log = LoggerFactory.getLogger(CustomNotifier.class);
    private final MessageService messageService;
    private final Properties properties;


    public CustomNotifier(InstanceRepository repository, MessageService messageService, Properties properties) {
        super(repository);
        this.messageService = messageService;
        this.properties = properties;
    }

    private static String template = "service name:%s(%s) \n\n state:%s(%s)";

    @Override
    protected Mono<Void> doNotify(InstanceEvent event, Instance instance) {
        return Mono.fromRunnable(() -> {
            if (event instanceof InstanceStatusChangedEvent) {
                log.info("Instance {} ({}) is {}", instance.getRegistration().getName(), event.getInstance(),
                        ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus());

                String status = ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus();
                String messageText;
                switch (status) {
                    // Health examination failed
                    case "DOWN":
                        log.info("Send the notice of failing health examination!");
                        messageText = String.format(template, instance.getRegistration().getName(), event.getInstance(), ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), "Health examination failed");
                        this.sendMessage(((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), instance.getRegistration().getServiceUrl(), "red", messageText);
                        break;
                    // Service offline
                    case "OFFLINE":
                        log.info("Send service offline notification!");
                        messageText = String.format(template, instance.getRegistration().getName(), event.getInstance(), ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), "Service offline");
                        this.sendMessage(((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), instance.getRegistration().getServiceUrl(), "red", messageText);
                        break;
                    //Service on-line
                    case "UP":
                        log.info("Send service online notice!");
                        messageText = String.format(template, instance.getRegistration().getName(), event.getInstance(), ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), "Service on-line");
                        this.sendMessage(((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), instance.getRegistration().getServiceUrl(), "green", messageText);
                        break;
                    // Service unknown exception
                    case "UNKNOWN":
                        log.info("Send service unknown exception notification!");
                        messageText = String.format(template, instance.getRegistration().getName(), event.getInstance(), ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), "Service unknown exception");
                        this.sendMessage(((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), instance.getRegistration().getServiceUrl(), "#F4ECF7", messageText);
                        break;
                    case "RESTRICTED":
                        log.info("Send service RESTRICTED exception notification!");
                        messageText = String.format(template, instance.getRegistration().getName(), event.getInstance(), ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), "Service RESTRICTED exception");
                        this.sendMessage(((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), instance.getRegistration().getServiceUrl(), "green", messageText);
                        break;
                    case "OUT_OF_SERVICE":
                        log.info("Send service OUT_OF_SERVICE exception notification!");
                        messageText = String.format(template, instance.getRegistration().getName(), event.getInstance(), ((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), "Service OUT_OF_SERVICE exception");
                        this.sendMessage(((InstanceStatusChangedEvent) event).getStatusInfo().getStatus(), instance.getRegistration().getServiceUrl(), "red", messageText);
                        break;
                    default:
                        break;
                }
            } else {
                log.info("Instance {} ({}) {}", instance.getRegistration().getName(), event.getInstance(),
                        event.getType());
            }
        });
    }

    //service name:oersi-backend(d287097a1ede)
// state:UP(Service on-line)
// service ip:http://NB-WXE15393.tib.tibub.de:8080/
    private void sendMessage(String serverStatus, String ip, String color, String message) {
        EmailSender emp = new EmailSender();
        emp.setEmailTo(properties.getEmailTo().split(","));
        emp.setSubjectMessage(
                properties.getDefaultSubject());
        emp.setEmailFrom(properties.getEmailFrom());
        emp.setTemplateName(properties.getFileNameTemplate());
        HashMap mapping = new HashMap<String, Object>();
        mapping.put("serverStatus", serverStatus);
        mapping.put("link", ip);
        mapping.put("color", String.format("background-color:%s;", color));
        mapping.put("message", message);
        emp.setBodyMessage(mapping);

        messageService.sendMessageWithTemplate(emp);
    }

}

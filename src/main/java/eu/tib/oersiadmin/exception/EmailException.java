package eu.tib.oersiadmin.exception;

import org.springframework.mail.MailException;

public class EmailException extends MailException {
  /*
   *
   */
  private static final long serialVersionUID = 1L;

  public EmailException(String message, Throwable cause) {
    super(message, cause);
  }

  public EmailException(String message) {
    super(message);
  }

}

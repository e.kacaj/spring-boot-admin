package eu.tib.oersiadmin.emailservice.service;


import eu.tib.oersiadmin.emailservice.configemail.EmailProperties;
import eu.tib.oersiadmin.exception.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;


@Service
public class MailTemplate {

    private static final Logger logger = LoggerFactory.getLogger(MailTemplate.class);
    private final EmailProperties emailProperties;

    private final TemplateEngine templateEngine;

    public MailTemplate(TemplateEngine templateEngine, EmailProperties emailProperties) {
        this.templateEngine = templateEngine;
        this.emailProperties = emailProperties;
    }


    public String getTemplates(Map<String, Object> mails, String language, String templateName) {
        logger.info("------------- Start MailTemplate class ---------------- ");
        Context context = new Context();
        String path;
        if (mails == null) {
            logger.info("---MailTemplate Error: missing data to translate in template ");
            throw new EmailException(" Error: missing data to translate in template ");
        }

        // convert the map to context
        context.setVariables(mails);

        if (StringUtils.isEmpty(language)) {
            path = emailProperties.getDefaultLanguage() + "/";
        } else {
            path = language + "/";
        }

        if (StringUtils.isEmpty(templateName)) {
            templateName = emailProperties.getFileNameTemplate();
        }

        return templateEngine.process(path + templateName, context);
    }


}

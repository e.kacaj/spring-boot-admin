package eu.tib.oersiadmin.emailservice.implementation;

import eu.tib.oersiadmin.emailservice.iemail.EmailSender;
import eu.tib.oersiadmin.emailservice.iemail.MessageService;
import eu.tib.oersiadmin.emailservice.iemail.Properties;
import eu.tib.oersiadmin.emailservice.service.MailTemplate;
import eu.tib.oersiadmin.exception.EmailException;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Component
public class EmailDefaultImpl implements MessageService {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EmailDefaultImpl.class);
  private final Properties emailProperties;
  private final JavaMailSender emailSender;
  private final MailTemplate mailTemplate;

  public EmailDefaultImpl(Properties emailProperties, JavaMailSender emailSender, MailTemplate mailTemplate) {
    this.emailProperties = emailProperties;
    this.emailSender = emailSender;
    this.mailTemplate = mailTemplate;
  }

  @Override
  public void sendMessageWithoutTemplate(EmailSender e, String bodyMessaeg) {
    logger.info(" start send email without templates ");
    SimpleMailMessage message = new SimpleMailMessage();
    message.setSubject(e.getSubjectMessage());
    message.setText(bodyMessaeg);
    message.setTo(e.getEmailTo());
    if (!e.getEmailFrom().isEmpty()) {
      message.setFrom(e.getEmailFrom());
    } else {
      message.setFrom(emailProperties.getEmailFrom());
    }
    emailSender.send(message);
    logger.info("Messages has been send :) ");
  }

  @Override
  public void sendMessageWithTemplate(EmailSender e) {
    logger.info("---------------- start send email with templates ----------------------------- ");
    MimeMessage message = emailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message);
    if (e == null) {
      throw new EmailException(" All fields are empty  ");
    }

    if (!emailProperties.getTemplateEnable()) throw new EmailException("Make true mail.template ");

    String templatesend = mailTemplate.getTemplates(e.getBodyMessage(), e.getLanguage(), e.getTemplateName());
    try {
      helper.setTo(e.getEmailTo());
      helper.setFrom(emailProperties.getEmailFrom());
      helper.setText(templatesend, true);
      helper.setSubject(e.getSubjectMessage());
    } catch (MessagingException exc) {
      throw new EmailException("Check the email validation", exc);
    }
    emailSender.send(message);
    logger.info(
            "---------------- successfully send email with templates ----------------------------- ");
  }


}

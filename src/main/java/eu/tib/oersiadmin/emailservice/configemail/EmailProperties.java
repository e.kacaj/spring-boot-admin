package eu.tib.oersiadmin.emailservice.configemail;


import eu.tib.oersiadmin.emailservice.iemail.Properties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
@ConfigurationProperties(prefix = "mail")
public class EmailProperties implements Properties {

  private String host;
  private String port;
  private String username;
  private String password;

  @Value("${mail.smtp.auth}")
  private Boolean mailSmtpAuth;

  @Value("${mail.starttls.enable}")
  private Boolean mailTslEnable;

  @Value("${mail.starttls.required}")
  private Boolean mailTslRequired;

  @Value("${mail.template}")
  private Boolean templateEnable;

  @Value("${mail.template.path}")
  private String pathTemplate;

  @Value("${mail.template.name}")
  private String fileNameTemplate;

  @Value("${mail.send.from}")
  private String emailFrom;

  @Value("${mail.send.to}")
  private String emailTo;

  @Value("${mail.default.language}")
  private String defaultLanguage;

  @Value("${mail.default.link}")
  private String defaultLink;

  @Value("${mail.send.subject.Default}")
  private String defaultSubject;

  private Map<String, String> emailSubject;


  public String getEmailFrom() {
    return emailFrom;
  }

  @Override
  public String getEmailTo() {
    return emailTo;
  }

  @Override
  public String getDefaultLanguage() {
    return defaultLanguage;
  }

  public String getPathTemplate() {
    return pathTemplate;
  }

  public String getFileNameTemplate() {
    return fileNameTemplate;
  }

  public Boolean getTemplateEnable() {
    return templateEnable;
  }

  public Boolean getMailSmtpAuth() {
    return mailSmtpAuth;
  }

  public Boolean getMailTslEnable() {
    return mailTslEnable;
  }

  public Boolean getMailTslRequired() {
    return mailTslRequired;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getHost() {
    return host;
  }

  public void setPort(String port) {
    this.port = port;
  }

  public String getPort() {
    return port;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getUsername() {
    return username;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public String getDefaultLink() {
    return defaultLink;
  }

  public void setDefaultLink(String defaultLink) {
    this.defaultLink = defaultLink;
  }


  @Override
  public Map<String, String> getEmailSubject() {
    return emailSubject;
  }

  public void setEmailSubject(Map<String, String> emailSubject) {
    this.emailSubject = emailSubject;
  }

  @Override
  public String getDefaultSubject() {
    return defaultSubject;
  }

  public void setDefaultSubject(String defaultSubject) {
    this.defaultSubject = defaultSubject;
  }
}

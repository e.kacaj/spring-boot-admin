package eu.tib.oersiadmin.emailservice.configemail;


import eu.tib.oersiadmin.exception.EmailException;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
@Configuration
public class EmailConfig {
  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EmailConfig.class);
  private final EmailProperties emailProperties;

  public EmailConfig(EmailProperties emailProperties) {
    this.emailProperties = emailProperties;
  }

  @Bean
  public JavaMailSender getMailSender() throws EmailException {
    logger.info("------------- Start EmailConfig class ---------------- ");
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost(emailProperties.getHost());
    mailSender.setPort(Integer.parseInt(emailProperties.getPort()));
    mailSender.setUsername(emailProperties.getUsername());
    mailSender.setPassword(emailProperties.getPassword());
    Properties javaMailProperties = new Properties();
    javaMailProperties.put("mail.smtp.starttls.enable", emailProperties.getMailTslEnable());
    javaMailProperties.put("mail.smtp.auth", emailProperties.getMailSmtpAuth());
    javaMailProperties.put("mail.transport.protocol", "smtp");
    javaMailProperties.put("mail.debug", "true");
    mailSender.setJavaMailProperties(javaMailProperties);
    logger.info("------------- End EmailConfig class ---------------- ");
    return mailSender;
  }


}

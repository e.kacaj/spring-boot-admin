package eu.tib.oersiadmin.emailservice.iemail;

import java.util.Map;

public interface Properties {
    String getPathTemplate();

    String getFileNameTemplate();

    Boolean getTemplateEnable();

    Boolean getMailSmtpAuth();

    Boolean getMailTslEnable();

    Boolean getMailTslRequired();

    String getHost();

    String getPort();

    String getUsername();

    String getPassword();

    String getEmailFrom();

    String getEmailTo();

    String getDefaultLanguage();

    String getDefaultLink();

    Map<String, String> getEmailSubject();

    String getDefaultSubject();


}

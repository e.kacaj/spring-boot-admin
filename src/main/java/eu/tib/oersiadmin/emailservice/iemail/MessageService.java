package eu.tib.oersiadmin.emailservice.iemail;


import eu.tib.oersiadmin.exception.EmailException;

public interface MessageService {
  void sendMessageWithoutTemplate(EmailSender e, String bodyMessage) throws EmailException;

  void sendMessageWithTemplate(EmailSender e) throws EmailException;
}

package eu.tib.oersiadmin.emailservice.iemail;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Map;

public class EmailSender {

  /*
   * this is a list for email
   */
  @NotNull(message = "Please provide an e-mail.")
  @NotEmpty(message = "The email field cannot be empty.")
  private String[] emailTo;
  /*
   * this is email from
   */
  private String emailFrom;

  /*
   * this is for subject messages
   */
  @NotNull(message = "Please provide an Subject Mesage.")
  @NotEmpty(message = "The Subject field cannot be empty.")
  private String subjectMessage;

  /*
   * this is for body message is Map because we need to convert the variables in values
   */
  private Map<String, Object> bodyMessage;

  private String language;

  private String templateName;

  public EmailSender() {
  }

  public EmailSender(String[] emailTo, String emailFrom, String subjectMessage,
                     Map<String, Object> bodyMessage, String language, String templateName) {
    this.emailTo = emailTo;
    this.emailFrom = emailFrom;
    this.subjectMessage = subjectMessage;
    this.bodyMessage = bodyMessage;
    this.language = language;
    this.templateName = templateName;
  }


  public String[] getEmailTo() {
    return emailTo;
  }

  public void setEmailTo(String[] emailTo) {
    this.emailTo = emailTo;
  }

  public String getEmailFrom() {
    return emailFrom;
  }

  public void setEmailFrom(String emailFrom) {
    this.emailFrom = emailFrom;
  }

  public String getSubjectMessage() {
    return subjectMessage;
  }

  public void setSubjectMessage(String subjectMessage) {
    this.subjectMessage = subjectMessage;
  }

  public Map<String, Object> getBodyMessage() {
    return bodyMessage;
  }

  public void setBodyMessage(Map<String, Object> bodyMessage) {
    this.bodyMessage = bodyMessage;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getTemplateName() {
    return templateName;
  }

  public void setTemplateName(String templateName) {
    this.templateName = templateName;
  }

  @Override
  public String toString() {
    return "EmailSender{" + "emailTo=" + Arrays.toString(emailTo) + ", emailFrom='" + emailFrom
            + '\'' + ", subjectMessage='" + subjectMessage + '\'' + ", bodyMessage=" + bodyMessage
            + ", language='" + language + '\'' + ", templateName='" + templateName + '\'' + '}';
  }


}
